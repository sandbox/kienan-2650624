<?php

/**
 * @file
 * Module file for the google_analytics_recover module.
 */

/**
 * Implements hook_permission().
 */
function google_analytics_recover_perm() {
  return array('administer google analytics recover settings');
}

/**
 * Implements hook_menu().
 */
function google_analytics_recover_menu() {
  $items['analytics.txt'] = array(
    'page callback' => 'google_analytics_recover_analyticstxt_callback',
    'type' => MENU_CALLBACK,
    'access callback' => TRUE,
  );
  $items['admin/config/google_analytics_recover'] = array(
    'page callback' => 'drupal_get_form',
    'page arguments' => array('google_analytics_recover_settings_form'),
    'title' => 'Google Analytics Recover Settings',
    'description' => 'Configure information required to help recover a google analytics account without filesystem access.',
    'access arguments' => array('administer google analytics recover settings'),
  );
  return $items;
}

/**
 * Page callback for the analytics.txt file.
 */
function google_analytics_recover_analyticstxt_callback() {
  $enabled = variable_get('google_analytics_recover_enabled', FALSE);
  $text = variable_get('google_analytics_recover_text', '');
  if (!$enabled || !$text) {
    drupal_not_found();
    return;
  }
  print $text;
  exit();
}

/**
 * Administration settings form for the Google Analytics Recover module.
 */
function google_analytics_recover_settings_form(&$form_state) {
  $form = array(
    'enable' => array(
      '#type' => 'checkbox',
      '#title' => t('Enable Google Analytics Recover'),
      '#description' => t('If enabled, the recovery text will be returned when /analytics.txt is accessed'),
      '#default_value' => variable_get('google_analytics_recover_enabled', FALSE),
    ),
    'recovery_text' => array(
      '#type' => 'textarea',
      '#title' => t('Recovery Text'),
      '#description' => t('Enter the contents of analytics.txt from Google here, along with any additional information required by Google.'),
      '#default_value' => variable_get('google_analytics_recover_text', ''),
    ),
    'submit' => array(
       '#type' => 'submit',
       '#value' => t('Submit'),
    ),
  );
  return $form;
}

/**
 * Submit handler for the google analytics recover module admin settings form.
 */
function google_analytics_recover_settings_form_submit($form, &$form_state) {
  if (isset($form_state['values']['enable'])) {
    variable_set('google_analytics_recover_enabled', $form_state['values']['enable']);
  }
  if (isset($form_state['values']['recovery_text'])) {
    variable_set('google_analytics_recover_text', $form_state['values']['recovery_text']);
  }
}
