# Summary #

This module is meant to give site adminisrators who may not have access to the file system, or site administrators on installations where it is inconvenient (eg. in aegir) to respond to google's analytics account recovery process.

Normally google asks users to place a file, analytics.txt in the web root of the domain.

This module implements a page callback for analytics.txt (which can be disabled), and allows an administrator with the relevant permission to modify the text it outputs.
